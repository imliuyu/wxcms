package com.weixun.admin.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.weixun.model.User;

import java.util.List;

/**
 * Created by myd on 2017/5/14.
 */
public class UserService {
    private static final long serialVersionUID = -1982696969221258167L;
//    private static final SysUser dao = new SysUser().dao();

    /**
     * 分页
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public Page<Record> paginate(int pageNumber, int pageSize,String user_name,String user_loginname,String user_phone,String user_email) {

        StringBuffer  sqlstr = new StringBuffer();
        sqlstr.append("from t_user s  where 1=1 ");
        if (user_name != null && !user_name.equals("")) {
            sqlstr.append("and s.user_name like '%" + user_name + "%' ");
        }
        if (user_loginname != null && !user_loginname.equals("")) {
            sqlstr.append("and s.user_loginname like '%" + user_loginname + "%' ");
        }
        if (user_phone != null && !user_phone.equals("")) {
            sqlstr.append("and s.user_phone like '%" + user_phone + "%' ");
        }
        if (user_email != null && !user_email.equals("")) {
            sqlstr.append("and s.user_email like '%" + user_email + "%' ");
        }
        sqlstr.append(" order by s.user_pk desc ");

        String select = "select *";
        return Db.paginate(pageNumber, pageSize, select,sqlstr.toString());
        
    }

    /**
     * 更具id查询
     * @param id
     * @return
     */
    public User findById(int id) {
        return User.dao.findById(id);
    }

    /**
     * 删除，可以根据多数据源删除
     * @param ids
     * @return
     */
    public int deleteById(String ids) {
        String sql = "delete from t_user where user_pk in ('"+ids+"')";
//        Db.delete("t_user","user_pk",ids);
        Integer result = Db.use("datasource").update(sql);
        return result;
    }

    /**
     * 数据添加
     * @param username
     * @param password
     * @return
     */
    public boolean add(String username,String password){
        String SQL = "SELECT user_pk FROM t_user WHERE user_name =?";
        Integer result = Db.queryFirst(SQL, username);
        boolean res=false;
        if(result==null){
            Record t_user = new Record();
            t_user.set("user_name", username);
            t_user.set("user_password", password);
            Db.save("t_user", t_user);
            res=true;
        }
        if(result==null){
            Record blog = new Record();
            blog.set("title", username);
            blog.set("content", password);
            Db.use("datasource2").save("blog", blog);
            res=true;
        }
        return res;
    }


    /**
     * 用户登录
     * @param username
     * @param password
     * @return
     */
    public boolean login(String username,String password){
        String SQL = "SELECT userid FROM t_user WHERE username =? and password=?";
        Integer result = Db.queryFirst(SQL, username, password);
        if(result!=null)
            return true;
        else return false;
    }

    /**
     * 更具用户名密码查找
     * @param username
     * @param password
     * @return
     */
    public List<Record> find(String username, String password)
    {
        String SQL = "SELECT * FROM t_user WHERE name =? and password=?";
        List<Record> list = Db.find(SQL,username, password);
        return list;
    }

    /**
     * 获取列表
     * @return
     */
    public List<Record> findList()
    {
        String sql = "select * from t_user";
        List<Record> list = Db.use("datasource").find(sql);
//        String sql = "select * from blog";
//        List<Record> list = Db.use("datasource2").query(sql);
        return list;
    }


}
